import {Socket} from "socket.io";
import Room from "../models/Room";
import jwt, {JwtPayload} from "jsonwebtoken";
import config from "../config";
import User from "../models/User";
import mongoose, {Types} from "mongoose";
import {IMusic, IPlayer, IRoom} from "../types/interfaces";
import Playlist from "../models/Playlist";
import Music from "../models/Music";
import {getStreamFlow} from "../helpers/musics";


const WAITING_TIME = 5;

const socketsAction =  (socket: Socket)  => {
    socket.on('join',(data) =>  join(data)(socket))
    socket.on('edit-config',(data) =>  editConfig(data)(socket))
    socket.on('guess',(data) =>  guess(data)(socket))
    socket.on('replay',() =>  replay(socket))
    socket.on("disconnect",(data) =>  disconnect(data)(socket))
}


const join = (data: any) => async (socket: Socket)   => {
    const {roomId, user } = data;

    let player : IPlayer | null = null;

    player = await getAuthPlayer(socket);

    if (!player) player = user;
    if (!player) return;

    const [room] = await Room.find({ _id: roomId });

    if (!room) return;

    const isAlreadyPlaying = room.players.find((p) => p._id.equals(mongoose.Types.ObjectId(player?._id)));
    if (!isAlreadyPlaying) {
        room.players = [...room.players, {...player, score: 0, _id: mongoose.Types.ObjectId(player._id)}];
        await room.save();
    }
    socket.join(roomId);
    socket.emit('roomState', room);
    socket.to(roomId).emit('roomState', room);

}


const editConfig = (data : IRoom["config"]) => async (socket : Socket) => {
    const { room_id } = socket.handshake.query;
    const player = await getAuthPlayer(socket);

    const [room] = await Room.find({_id: room_id});

    if( room_id && player && room &&  player._id.equals(room.admin)) {
        room.config = {...data};
        await room.save();
        socket.emit('roomState', room);
        socket.to(room_id).emit('roomState', room);
    }
    if (data?.started) {
        start(socket);
    }
}


const guess = (data : IRoom["chat"][0]) => async (socket : Socket) => {
    const { room_id } = socket.handshake.query;
    const [room] = await Room.find({_id: room_id}).select('+currentMusic._id +currentMusic.timestamp');
    const player = room?.players?.find(p => p._id.equals(data.sender));
    const [hasFound, score] = await foundMusic(room, data);
    if (hasFound && score && player && room_id) {
        player.score = player.score + score;
        room.players = [...room.players];
        room.currentMusic = room.currentMusic &&  {...room.currentMusic, found: [...room.currentMusic.found, {...player, score}]}
        room.markModified('players');
        room.markModified('currentMusic');
        await room.save();
        const [newRoom] = await Room.find({_id: room_id})
        socket.emit('roomState', newRoom);
        socket.to(room_id).emit('roomState', newRoom);
    }

    if( !hasFound && room_id  && player ) {
        room.chat = [data,...room.chat];
        await room.save();
        socket.emit('roomState', room);
        socket.to(room_id).emit('roomState', room);
    }
}

const getAuthPlayer = async (socket: Socket) : Promise<IPlayer | null> => {
    const {token} = socket.handshake.auth;
    if (token) {
        const payload =  jwt.verify(token, config.jwt.secret) as JwtPayload;
        const [userDatabase]  = await User.find({_id: payload?.id},["_id","username","avatar"])
        if (userDatabase ) {
            return ({
                _id: userDatabase._id,
                username: userDatabase.username,
                avatar: userDatabase.avatar,
                score: 0
            });
        }
    }
    return null;
}


const replay = async (socket: Socket) => {
    const { room_id } = socket.handshake.query;
    const player = await getAuthPlayer(socket);

    const [room] = await Room.find({_id: room_id});

    if( room_id && player && room &&  player._id.equals(room.admin)) {
        room.players = [...room.players.map((p) => ({...p, score: 0}))];
        room.currentMusic = undefined;
        room.config = {
            started: false,
            playlist: room.config.playlist,
            time: room.config.time,
            finished: false
        };
        room.markModified('players');
        room.markModified('config');
        room.markModified('currentMusic');
        await room.save();
        socket.emit('roomState', room);
        socket.to(room_id).emit('roomState', room);
    }
}

const disconnect = (reason: string) => async (socket: Socket) => {
    const {user_id , room_id} = socket.handshake.query;
    const [room] = await Room.find({_id: room_id});
    if(!room_id || !room || !user_id || typeof user_id !== 'string') return;
    room.players = [...room.players.filter((player) => {
        return !player._id.equals(mongoose.Types.ObjectId(user_id))
    })]
    await room.save();
    socket.emit('roomState', room);
    socket.to(room_id).emit('roomState', room);
    if( !room?.players.length) {
        setTimeout(async () => {
            const [room] = await Room.find({_id: room_id});
            if(!room?.players.length) {
                room.delete();
            }
        },2000);
    }
}


const start = async (socket: Socket) => {
    const  room_id  = socket.handshake.query.room_id as string;
    const room = await Room.findById(room_id);

    if (!room) {
        return
    }

    const playlist = await Playlist.findById(room.config.playlist);

    if (!playlist) {
        return
    }

    const shuffleMusics =  playlist.musics.sort(() => 0.5 - Math.random());
    launchMusic(socket)(shuffleMusics);
}

const launchMusic = (socket: Socket) =>  async (shuffleMusics: IMusic["_id"][], index: number = 0) => {
    const  room_id  = socket.handshake.query.room_id as string;
    const room = await Room.findById(room_id);
    if (!room) return;
    if (index >= shuffleMusics.length ) {
        room.config = {...room.config, finished : true}
        room.markModified('config');
        await room.save();
        socket.to(room_id).emit('roomState', room);
        socket.emit('roomState', room);
        return
    };
    const musicId = shuffleMusics[index];
    const music = await Music.findById(musicId);
    if (music) {
        const stream = await getStreamFlow(music, room);
        room.currentMusic = {
            _id: musicId,
            timestamp: Date.now(),
            found: [],
            currentPlayed: index + 1,
            nbMusics: shuffleMusics.length
        };
        room.markModified('currentMusic');
        await room.save();
        socket.to(room_id).emit('roomState', room);
        socket.emit('roomState', room);
        socket.to(room_id).emit('data-music', stream);
        socket.emit('data-music', stream);
        await waiting(room.config.time );
        await waiting(WAITING_TIME)
    }
    await launchMusic(socket)(shuffleMusics, index + 1)
}

const waiting = (seconds: number) =>  new Promise<void>((resolve) => {
    setTimeout(() => resolve(), seconds * 1000);
})


const foundMusic = async (room: IRoom, data: IRoom["chat"][0]) : Promise<[boolean, number]> => {
    const {sender, message : guess} = data;
    if(room.currentMusic?._id) {
        const [music] = await Music.find({_id: room.currentMusic._id})
        if (music) {
            const {keywords = [], title, artists} = music;
            const poolGuess = [...keywords, title, ...artists ];

            if(poolGuess.map(val => val.toLowerCase().trim()).includes(guess.toLowerCase().trim())) {
                const hasAlreadyFound = !!room.currentMusic?.found.find((player: IPlayer) => {
                    return player?._id?.equals(sender);
                })
                if (hasAlreadyFound) return [true, 0];
                const nbPlayer = room.players?.length;
                const playerRank = room.currentMusic?.found?.length;
                const timeAvailable = room.config?.time;
                const timeToFind = room.currentMusic?.timestamp && (Date.now() - room.currentMusic?.timestamp) / 1000;
                const scorePlayer = 1 - ((playerRank - 1)/nbPlayer);
                const scoreTime = timeToFind ?  1 - (timeToFind/(timeAvailable + 2)) : 1;
                let scoreRank = 0;
                if (playerRank === 1) scoreRank = 20;
                if (playerRank === 2) scoreRank = 10;
                if (playerRank === 3) scoreRank = 5;
                return [true,Math.max(Math.round((scorePlayer * scoreTime * 100) + 10 + scoreRank), 0)];
            }
        }
    }
    return [false, 0]
}


export default socketsAction;
