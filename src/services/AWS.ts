import * as AWS from 'aws-sdk';
import {GetObjectOutput, GetObjectRequest, PutObjectRequest} from 'aws-sdk/clients/s3';
import * as fs from 'fs';
import {AWSError} from "aws-sdk";

const s3 = new AWS.S3({
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: process.env.AWS_BUCKET_REGION,
})

const uploadFile = async (file: any) => {
  const fileObject = fs.readFileSync(file.path)

  const uploadParams: PutObjectRequest = {
    Bucket: process.env.AWS_BUCKET_NAME!,
    Key: file.originalname,
    Body: fileObject
  }

  let uploadFile = await s3.upload(uploadParams, (err, data) => {
    if (err) {
      console.log(err);
    }
    console.log(data)
    return data
  })

  return uploadFile.promise();
};


const getFile = async (filename: string): Promise<GetObjectOutput> => {
  const downloadParams: GetObjectRequest = {
    Bucket: process.env.AWS_BUCKET_NAME!,
    Key: filename,
  }
  return new Promise((resolve, reject) => {
     s3.getObject(downloadParams, (err, data) => {
      if (err) {
        reject(err);
      }
      resolve(data)
    })
  })
};

export const readStream = (filename: string, callback: (err: AWSError ,data: GetObjectOutput) => any) => {
  console.log(filename);
  const downloadParams: GetObjectRequest = {
    Bucket: process.env.AWS_BUCKET_NAME!,
    Key: filename,
  }

  return s3.getObject(downloadParams, callback).createReadStream();

}

export {
  uploadFile,
  getFile
}
