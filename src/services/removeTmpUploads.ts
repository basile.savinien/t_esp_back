import * as fs from 'fs';
import * as path from 'path';

const removeAllTmpUploads = async () => {
  fs.readdir('./uploads/tmp', (err, files) => {
      if (err) {
          console.log(err);
      }
  
      files.forEach(file => {
          const fileDir = path.join('./uploads/tmp', file);
  
          if (file !== '.gitkeep') {
              fs.unlinkSync(fileDir);
          }
      });
  });
};

export {
  removeAllTmpUploads,
}
