import mongoose from 'mongoose';
import config from '../config';
import { IRoom } from '../types/interfaces';

const RoomSchema = new mongoose.Schema<IRoom>(
  {
    players: [],
    private: {
      type: Boolean,
      default: false,
    },
    playlist: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Playlist',
      },
      config: {
        type: Object
      },
    chat: {
      type: Array,
      default: [],
    },
    admin: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
      currentMusic : {
        _id: {
            select: false,
            type: mongoose.Schema.Types.ObjectId
        },
          timestamp: {
            select: false,
            type: Number
          },
          found : [{
              username: String,
              score: Number,
              avatar: Object,
              _id: mongoose.Schema.Types.ObjectId
          }],
          currentPlayed: {
            type: Number
          },
          nbMusics: {
            type: Number
          }
      }
  },
  {
    timestamps: true,
  }
);

const Room = mongoose.model<IRoom>('Room', RoomSchema);

export default Room;
