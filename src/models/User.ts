import mongoose, { HookNextFunction } from 'mongoose'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import isEmail from 'validator/lib/isEmail'
import { IUser } from '../types/interfaces'
import config from '../config'
import { UserRole } from '../types/enums'

const UserSchema = new mongoose.Schema<IUser>({
  email: {
    type: String,
    validate: [isEmail, 'Please provide a valid email address.'],
    required: [true, 'Email is required'],
    unique: true
  },
  password: {
    type: String,
    required: [true, 'Password is required'],
    minLength: 8
  },
  username: {
    type: String,
    required: [true, 'Username is required'],
    unique: true
  },
  avatar: {
    type: Object,
  },
  role: {
    type: String,
    enum: UserRole,
    default: UserRole.USER,
  },
  playlistSaved: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Music'
    }
  ],
  spotify_token: {
    type: String
  },
  platformAccounts: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'PlatformAccount',
    },
  ],
  sockets: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Socket',
    },
  ],
}, {
  timestamps: true
})

UserSchema.pre<IUser>('save', async function(next: HookNextFunction) {
  if (!this.isModified('password')) {
    next()
  }

  try {
    const salt = await bcrypt.genSalt(10)
    this.password = await bcrypt.hash(this.password, salt)

    return next()
  } catch(e: any) {
    next(e)
  }
})

UserSchema.method('checkPassword', function(password: string): boolean {
  return bcrypt.compareSync(password, this.password)
})

UserSchema.method('generateJWT', function(): Nullable<string> {
  const payload = {
    id: this.id,
    email: this.email
  }

  return jwt.sign(payload, config.jwt.secret as string, {
    expiresIn: config.jwt.expirationTime
  })
})

UserSchema.method('verifyEmail', function (email: string): boolean {
  const emailRegex = new RegExp(
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  );

  return emailRegex.test(email);
});

UserSchema.static('checkExistingField', async (field, value) => {
  const checkField = await User.findOne({ [field]: value })

  return checkField
})

const User = mongoose.model<IUser>('User', UserSchema)

export default User
