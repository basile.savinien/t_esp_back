import mongoose from 'mongoose';
import config from '../config';
import { PlatformType } from '../types/enums';
import { IPlatformAccount } from '../types/interfaces';

const PlatformAccountSchema = new mongoose.Schema<IPlatformAccount>(
  {
    platform: {
      type: PlatformType,
      unique: true,
    },
    apiKey: {
      type: String,
      unique: true,
    },
  },
  {
    timestamps: true,
  }
);

const PlatformAccount = mongoose.model<IPlatformAccount>('PlatformAccount', PlatformAccountSchema);

export default PlatformAccount;
