import mongoose from 'mongoose'
import { IGame } from '../types/interfaces'
import config from '../config'

const GameSchema = new mongoose.Schema<IGame>({
  
}, {
  timestamps: true
})

const Game = mongoose.model<IGame>('Game', GameSchema)

export default Game