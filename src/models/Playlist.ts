import mongoose from 'mongoose';
import config from '../config';
import { PlatformType } from '../types/enums';
import { IPlaylist } from '../types/interfaces';

const PlaylistSchema = new mongoose.Schema<IPlaylist>(
  {
    name: {
      type: String,
    },
    private: {
      type: Boolean,
    },
    type: {
      type: String,
    },
    image: {
      type: String,
    },
    musics: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Music',
      },
    ],
    length: {
        type: Number
    },
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
  },
  {
    timestamps: true,
  }
);

const Playlist = mongoose.model<IPlaylist>('Playlist', PlaylistSchema);

export default Playlist;
