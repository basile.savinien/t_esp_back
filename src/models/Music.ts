import mongoose from 'mongoose';
import config from '../config';
import { IMusic } from '../types/interfaces';

const MusicSchema = new mongoose.Schema<IMusic>(
  {
    title: {
      type: String,
      unique: true,
    },
    album: {
      type: String,
    },
    type: {
      type: String,
    },
    dateCreation: {
      type: Date,
    },
    image: {
      type: String,
    },
    durationSecond: {
      type: Number,
    },
    filename: {
      type: String,
    },
    path: {
      type: String,
    },
    artists: [{
      type: String,
    }],
    keywords: [{
        type: String
    }],
    playlists: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Playlist',
      },
    ],
  },
  {
    timestamps: true,
  }
);

const Music = mongoose.model<IMusic>('Music', MusicSchema);

export default Music;
