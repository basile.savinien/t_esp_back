import mongoose from 'mongoose';
import config from '../config';
import { ISocket } from '../types/interfaces';

const SocketSchema = new mongoose.Schema<ISocket>(
  {
    room: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Room',
    },
    token: {
      type: String,
      required: true,
    },
    users: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Room',
      },
    ],
  },
  {
    timestamps: true,
  }
);

const Socket = mongoose.model<ISocket>('Socket', SocketSchema);

export default Socket;
