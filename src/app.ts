import express, {RequestHandler, Response} from 'express';
import cors from 'cors';
import helmet from 'helmet';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import mongoose from 'mongoose';
import { createServer } from "http";
import { Server } from "socket.io";

import { AuthRoutes, GameRoutes, RoomRoutes, UserRoutes, MusicRoutes, PlaylistRoutes } from './routes';
import socketsAction from "./sockets/rooms";

export default class App {
  public app: express.Application = express();
  public router: express.Router = express.Router();
  public httpServer = createServer(this.app);
  public io = new Server(this.httpServer, {
    cors: {
      origin: process.env.CORS_SOCKET,
    }
  });

  constructor() {
    this.configApp();
    this.setupDatabase();
    this.setRoutes();
    this.setSockets()
  }

  private configApp() {
    this.app.use(helmet());
    this.app.use(cors());
    this.app.use(cookieParser());
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: false }));

    this.app.use('/', this.router);
  }

  private setupDatabase() {
    mongoose
      .connect(process.env.DB_URI!, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false,
      })
      .then(() => {
        console.log('Successfully connect to MongoDB.');
      })
      .catch((err) => {
        console.error('Connection error', err);
        process.exit()
      });
  }

  private setRoutes() {
    this.router.get('/', (_, res: Response) => {
      res.status(200).send('MuzikoIO API');
    });

    this.router.use('/', AuthRoutes);
    this.router.use('/games', GameRoutes);
    this.router.use('/playlists', PlaylistRoutes);
    this.router.use('/musics', MusicRoutes);
    this.router.use('/users', UserRoutes);
    this.router.use('/rooms', RoomRoutes);
  }

  private setSockets() {
    this.io.of("rooms").on("connection", socketsAction);
  }

  public run() {
    const port = process.env.PORT || 8081;
    this.httpServer.listen(port);
  }
}
