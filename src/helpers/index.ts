import { Request } from 'express'
import {IPlatformAccount, ISocket, IUser} from "@/types/interfaces";
import {UserRole} from "@/types/enums";

const extractToken = (req: Request) => {
  if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
    return req.headers.authorization.split(' ')[1]
  }
}

const cleanUser = (user: IUser) => {
  return({
    _id: user._id,
    username: user.username,
    email: user.email,
    icon: user.icon,
    role: user.role,
    avatar: user.avatar,
    sockets: user.sockets
  })
}

export {
  extractToken,
    cleanUser
}
