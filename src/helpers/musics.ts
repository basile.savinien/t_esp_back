import {getFile} from "../services/AWS";
import fs from "fs";
import path from "path";
import {IMusic, IRoom} from "../types/interfaces";

const chunks = require('buffer-chunks');

const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path;
const ffmpeg = require('fluent-ffmpeg');
ffmpeg.setFfmpegPath(ffmpegPath);

export const getStreamFlow = async (music: IMusic, room: IRoom) => {
    return new Promise<Buffer[]>(async (resolve, reject) => {
        const PATH_FILE = path.resolve(process.cwd(), 'stream/tmp' ,`${room._id}_temp.mp3`);
        const PATH_FILE_DEST = path.resolve(process.cwd(), 'stream/tmp' ,`${room._id}_temp_resized.mp3`);

        const file = await getFile(music.filename);
        await fs.writeFileSync(PATH_FILE, file?.Body as string);

        await ffmpeg()
            .input(PATH_FILE)
            .duration(room.config.time)
            .output(PATH_FILE_DEST)
            .on('error', function(err: any) {
                console.log('An error occurred: ' + err.message);
            })
            .on('end', async function () {
                console.log('Processing finished !');
                const final_file = await  fs.readFileSync(PATH_FILE_DEST);

                fs.unlinkSync(PATH_FILE);
                fs.unlinkSync(PATH_FILE_DEST);
                resolve(chunks(final_file, Math.round(final_file.length / room.config.time)));
            })
            .run();
    })
}
