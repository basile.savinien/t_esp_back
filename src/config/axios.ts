import axios from 'axios'

const spotifyInstance = axios.create({
  baseURL: 'https://api.spotify.com',
  headers: {
    Authorization: `Bearer `
  }
})

export {
  spotifyInstance
}
