import { Document, Types } from 'mongoose'
import { PlatformType, UserRole } from './enums'

export interface IUser extends Document {
  username: string
  password: string
  email: string
  icon: string
  avatar: object
  playlistSaved: Types.ObjectId[]
  role: UserRole
  sockets: ISocket[]
  platformAccounts: IPlatformAccount[]

  checkPassword(password: string): string
  verifyEmail(email: string): boolean
  generateJWT():string
  checkExistingField(field: string, value: string | number): string
}

export interface IGame extends Document {

}

export type IPlayer = Pick<IUser, "_id" | "username" | "avatar" > & {score: number};

interface IConfigPlaylist {
  time: number
  started: boolean
  playlist: IPlaylist["_id"] | null,
  finished: boolean
}

interface CurrentMusic {
  nbMusics?: number
  currentPlayed?: number
  _id?: IMusic["_id"]
  timestamp?: number
  found: IPlayer[]
}

interface Imessage {
  sender: IPlayer["_id"]
  message: string
  username: IPlayer["username"]
}

export interface IRoom extends Document {
  players:IPlayer[]
  private: boolean
  playlist: IPlaylist
  config: IConfigPlaylist
  admin: IPlayer["_id"]
  chat: Imessage[]
  currentMusic: CurrentMusic | undefined;
}

export interface IPlatformAccount extends Document {
  platform: PlatformType
  apiKey: string
}

export interface IPlaylist extends Document {
  name: string,
  image: string,
  musics: IMusic[],
}

export interface IMusic extends Document {
  title: string,
  artists: string,
  album: string,
  image: string,
  dateCreation: Date,
  keywords: string[]
  filename: string,
  path: string,
  durationSecond: number,
  playlists: IPlaylist[]
}

export interface ISocket extends Document {
  room: string
  token: string
  users: IUser[]
}

export interface ISocket extends Document {
  room: string
  token: string
  users: IUser[]
}

export interface IMessage {
  sender: Types.ObjectId,
  message: string
}
