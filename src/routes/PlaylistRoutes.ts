import { Router } from 'express'
import PlaylistController from '../controllers/PlaylistController'
import { checkJWT } from '../middlewares/checkJWT'
import checkRole from '../middlewares/checkRole'
import { UserRole } from '../types/enums'

const router = Router()

router.post('/', [checkJWT], PlaylistController.createPlaylist.bind(PlaylistController))
router.post('/save/:id',[checkJWT, PlaylistController.savePlaylist.bind(PlaylistController)])

router.get('/', PlaylistController.getPlaylists.bind(PlaylistController))
router.get('/me',[checkJWT], PlaylistController.getMyPlaylists.bind(PlaylistController))
router.get('/saved', [checkJWT], PlaylistController.getSavedPlaylists.bind(PlaylistController))
router.get('/:id', PlaylistController.getPlaylistById.bind(PlaylistController))

router.patch('/:id', [checkJWT, checkRole([UserRole.MODERATOR, UserRole.ADMIN])], PlaylistController.updatePlaylistById.bind(PlaylistController))

router.delete('/:id', [checkJWT, checkRole([UserRole.MODERATOR, UserRole.ADMIN])], PlaylistController.deletePlaylist.bind(PlaylistController))

export default router;
