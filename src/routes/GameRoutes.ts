import { Router } from 'express'
import { GameController } from '../controllers'

const router = Router()

router.get('/', GameController.getGames.bind(GameController))

router.get('/:id', GameController.getGameById.bind(GameController))

// router.get('/themes', GameController.getThemes.bind(GameController))

export default router