import { Router } from 'express';
import { UserController } from '../controllers';
import { checkJWT } from '../middlewares/checkJWT';
import checkRole from '../middlewares/checkRole';
import { UserRole } from '../types/enums';

const router = Router();

router.get('/me', [checkJWT], UserController.getMe.bind(UserController));
router.patch('/me', [checkJWT], UserController.editMe.bind(UserController));

router.get('/', [checkJWT], UserController.getUsers.bind(UserController));

router.get('/:id', [checkJWT, checkRole([UserRole.MODERATOR, UserRole.ADMIN])], UserController.getUserById.bind(UserController));

router.patch('/:id', [checkJWT, checkRole([UserRole.ADMIN])], UserController.editUserById.bind(UserController));

router.delete('/:id', [checkJWT, checkRole([UserRole.ADMIN])], UserController.deleteUserById.bind(UserController));

router.post('/validateAnonymous', UserController.ValidateTempUser.bind(UserController));

export default router;
