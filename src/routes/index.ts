import AuthRoutes from './AuthRoutes'
import GameRoutes from './GameRoutes'
import RoomRoutes from './RoomRoutes'
import UserRoutes from './UserRoutes'
import MusicRoutes from './MusicRoutes'
import PlaylistRoutes from './PlaylistRoutes'


export {
  AuthRoutes,
  GameRoutes,
  RoomRoutes,
  UserRoutes,
  MusicRoutes,
  PlaylistRoutes
}