import { Router } from 'express'
import { RoomController } from '../controllers'
import {checkJWT} from '../middlewares/checkJWT';

const router = Router()

router.post('/',[checkJWT], RoomController.createRoom.bind(RoomController))
router.post('/:id', RoomController.addPlaylist.bind(RoomController))

router.post('/:id', RoomController.enterInRoom.bind(RoomController))
router.post('/:id', RoomController.leaveRoom.bind(RoomController))

router.get('/', RoomController.getRooms.bind(RoomController))
router.get('/:id', RoomController.getRoomById.bind(RoomController))

router.delete('/:id', RoomController.deleteRoom.bind(RoomController))

export default router
