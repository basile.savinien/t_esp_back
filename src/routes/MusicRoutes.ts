import { Router } from 'express'
import { MusicController } from '../controllers'
import { checkJWT } from '../middlewares/checkJWT'
import checkRole from '../middlewares/checkRole'
import upload from '../middlewares/requestFiles'
import { UserRole } from '../types/enums'

const router = Router()

// router.get('/profileID', MusicController.profileID.bind(MusicController))

router.post('/', [checkJWT, checkRole([UserRole.MODERATOR, UserRole.ADMIN]), upload.single('file')], MusicController.createMusic.bind(MusicController))

router.get('/', MusicController.getMusics.bind(MusicController))
router.get('/:id', MusicController.getMusicById.bind(MusicController))

router.patch('/:id', [checkJWT, checkRole([UserRole.MODERATOR, UserRole.ADMIN])], MusicController.updateMusicById.bind(MusicController))

router.delete('/:id', [checkJWT, checkRole([UserRole.MODERATOR, UserRole.ADMIN])], MusicController.deleteMusic.bind(MusicController))


export default router
