import { Router } from 'express'
import { AuthController } from '../controllers'
import { checkJWT } from '../middlewares/checkJWT';

const router = Router()

router.post('/login', AuthController.login.bind(AuthController))

router.post('/register', AuthController.register.bind(AuthController))

router.get('/spotify', [checkJWT], AuthController.spotify_login.bind(AuthController))

router.get('/callback', [checkJWT], AuthController.callback.bind(AuthController))

export default router