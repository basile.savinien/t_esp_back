import { Response } from 'express'
import { StatusCode } from '../types/enums'

export default class RequestController {
  static sendSuccess(res: Response, message?: string): Response {
    return res.status(StatusCode.OK).send(message)
  }

  static sendSuccessJSON(res: Response, data: object): Response {
    return res.status(StatusCode.OK).json(data)
  }

  static sendCreationSuccess(res: Response, message?: string): Response {
    return res.status(StatusCode.CREATED).send(message)
  }

  static sendError(res: Response, message?: any) {
    return res.status(StatusCode.BAD_REQUEST).send(message)
  }

  static sendUnauthorized(res: Response) {
    return res.status(StatusCode.UNAUTHORIZED).send('Unauthorized')
  }

  static sendNotFound(res: Response) {
    return res.status(StatusCode.NOT_FOUND).send('Not Found')
  }

  static sendAlreadyExist(res: Response) {
    return res.status(StatusCode.CONFLICT).send('Resource already exists.')
  }

  static sendServerError(res: Response) {
    return res.status(StatusCode.SERVER_ERROR).send('Server Error')
  }
}