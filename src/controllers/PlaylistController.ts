import {Types} from 'mongoose';
import { Request, Response } from 'express';
import Playlist from '../models/Playlist';
import RequestController from './RequestController';
import User from "../models/User";

export default class PlaylistController extends RequestController {
  /**
   * Create a playlist
   *
   * @param req
   * @param res
   * @returns
   */
  static async createPlaylist(req: Request, res: Response) {
    const { name, isPrivate, musics, type } = req.body;
    const id = res.locals.jwtPayload.id;
    try {
      let user = await User.findOne({ _id: id }).exec();

      if (!user) {
        return this.sendError(res, 'User not found');
      }

      const playlist = await Playlist.create({
        name,
        private: isPrivate,
        type,
        musics,
        length: musics.length,
        creator: user._id
      });

      return this.sendSuccessJSON(res, playlist);
    } catch (e) {
      console.log(e);
      return this.sendError(res, 'error');
    }
  }


  static async savePlaylist(req: Request, res: Response) {
    const id = res.locals.jwtPayload.id;
    const playlist = Types.ObjectId(req.params.id);
    try {
      let user = await User.findOne({ _id: id }).exec();
      if (!user) {
        return this.sendError(res, 'User not found');
      }
      if (user.playlistSaved.includes(playlist)) {
        user.playlistSaved = user.playlistSaved.filter((listId) => !listId.equals(playlist));
      } else {
        user.playlistSaved = [...user.playlistSaved, playlist];
      }
      await user.save();
      return this.sendSuccessJSON(res, {playlistSaved: user.playlistSaved});
    } catch (e) {
      console.log(e);
      return this.sendError(res, 'error');
    }
  }

  /**
   * Get all playlists
   *
   * @param req
   * @param res
   * @returns
   */
  static async getPlaylists(req: Request, res: Response) {
    try {
      const filters = await this.getFiltersPlaylists(req);
      const playlists = await Playlist.find(filters);

      if (!playlists) {
        return this.sendServerError(res);
      }

      this.sendSuccessJSON(
        res,
        playlists
      );

    } catch (e) {
      console.log(e);
      return this.sendError(res, 'error');
    }
  }

  static async getSavedPlaylists(req: Request, res: Response) {
    const userId = res.locals.jwtPayload.id;
    try {
      let user = await User.findOne({ _id: userId }).exec();
      if (!user) {
        return this.sendError(res, 'User not found');
      }
      req.query.arrayIds = user.playlistSaved.map((id) => id.toString());
      return PlaylistController.getPlaylists(req, res);
    } catch (e) {
      return this.sendError(res, "error");
    }
  }

  static async getMyPlaylists(req: Request, res: Response) {
    req.query.creator = res.locals.jwtPayload.id;
    return PlaylistController.getPlaylists(req, res)
  }

  static async getFiltersPlaylists (req: Request) {
    const { name, type, rangeMin, rangeMax, creator , arrayIds  } = req.query;
    return  Object.entries({
      _id: arrayIds,
      name: name && {$regex: name, $options: 'i'},
      type: type,
      length : { ...(rangeMin || rangeMax) &&
        {
            ...(rangeMin) && {$gte: parseInt?.(rangeMin as string)},
            ...(rangeMax) && {$lte: parseInt?.(rangeMax as string)}
          }
        },
      creator: creator && creator,

      }).reduce((acc,[key, val]) => {
        if(key === '_id') {
          return({
            ...acc,
            ... (val) && {
              [key]: val
            }
          })
        }
        const param = (val && Object.keys(val || {})?.length  ) && {[key]: val}
        return ({
          ...acc,
          ...param
        })
    },{});
  }

  /**
   * Get a playlist
   *
   * @param req
   * @param res
   * @returns
   */
  static async getPlaylistById(req: Request, res: Response) {
    try {
      const {populate} = req.query;
      const playlist = await (Playlist.findById(req.params.id).populate([...populate === 'true' ? ['musics'] : []]));

      if (playlist) {
        return this.sendSuccessJSON(res, playlist);
      }

      return this.sendError(res, 'error');
    } catch (e) {
      console.log(e);
      return this.sendError(res, 'error');
    }
  }

  /**
   * Update a playlist
   *
   * @param req
   * @param res
   * @returns
   */
  static async updatePlaylistById(req: Request, res: Response) {

    try {
      const playlistUpdated = await Playlist.findOneAndUpdate({ _id: req.params.id }, {
        ...req.body,
        ...req.body.musics && {
          length: req.body.musics?.length,
        }
      }, { new: true });
      console.log(playlistUpdated);

      if (playlistUpdated) {
        console.log(playlistUpdated);
        return this.sendSuccessJSON(res, playlistUpdated);
      }
      return this.sendError(res, 'error');
    } catch (e) {
      return this.sendError(res, 'error');
    }
  }

  /**
   * Delete a playlist
   *
   * @param req
   * @param res
   * @returns
   */
  static async deletePlaylist(req: Request, res: Response) {
    try {
      const playlistDeleted = await Playlist.deleteOne({ _id: req.params.id });

      // TODO : remove playlist in all music playlist field

      if (playlistDeleted) {
        return this.sendSuccessJSON(res, { status: 'deleted' });
      }
      return this.sendError(res, 'error');
    } catch (e) {
      console.log(e);
      return this.sendError(res, 'error');
    }
  }
}
