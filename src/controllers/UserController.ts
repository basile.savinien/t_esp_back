import { Request, Response } from 'express'
import User from '../models/User'
import RequestController from './RequestController'
import mongoose from 'mongoose';

import {cleanUser} from "../helpers";

export default class UserController extends RequestController {

  /**
   * find a user
   *
   * @param req
   * @param res
   * @returns
   */
  static async getMe(req: Request, res: Response) {
    const id = res.locals.jwtPayload.id;

    try {
      let user = await User.findOne({ _id: id }).exec();

      if (!user) {
        return this.sendError(res, 'User not found');
      }

      this.sendSuccessJSON(res, { user });
    } catch (e) {
      return this.sendError(res, "error");
    }
  }

  /**
   * Edit current user
   *
   * @param req
   * @param res
   * @returns
   */
  static async editMe(req: Request, res: Response) {
    const id = res.locals.jwtPayload.id;
    const { email ,oldPassword, newPassword } = req.body;

    try {
      let user = await User.findOne({ _id: id }).exec();
      console.log(user);

      if (email && !user?.verifyEmail(email)) {
        return this.sendError(res, 'Email is not correct');
      }

      if (newPassword && !user?.checkPassword(oldPassword)) {
        return this.sendError(res, 'Old password is not correct');
      }

      // TODO : if user role is user and try to change it => keep role user

      const userUpdated = await User.findOneAndUpdate({ _id: id }, req.body, { new: true });

      if (userUpdated) {
        console.log(userUpdated);
        return this.sendSuccessJSON(res, cleanUser(userUpdated));
      }
      return this.sendError(res, 'error');
    } catch (e) {
      return this.sendError(res, 'error');
    }
  }

  /**
   * Get a user
   *
   * @param req
   * @param res
   * @returns
   */
  static async getUserById(req: Request, res: Response) {
    try {
      let user = await User.findOne({ _id: req.params.id }).exec()

      if (!user) {
        return this.sendError(res, 'User not found')
      }

      this.sendSuccessJSON(res, { user })

    } catch(e) {
      return this.sendError(res, 'error')
    }
  }

  /**
   * Validate temp user
   *
   * @param req
   * @param res
   * @returns
   */
  static async ValidateTempUser(req: Request, res: Response) {
    const { username } = req.body;
    try {
      let users = await User.find({username}).exec();

      if (!!users.length) {
        return this.sendAlreadyExist(res)
      }
      const _id = new mongoose.Types.ObjectId();
      this.sendSuccessJSON(res, {
        success: true,
        user: {
          _id,
          username
        }
      })
    } catch (e) {
      console.log(e);
      return this.sendError(res, 'error')
    }
  }

  /**
   * Get all users
   *
   * @param req
   * @param res
   * @returns
   */
  static async getUsers(req: Request, res: Response) {
    try {
      let users = await User.find({}).exec()

      if (!users) {
        return this.sendServerError(res)
      }

      this.sendSuccessJSON(res, users.map(e => {
        return {
          id: e.id,
          username: e.username,
          email: e.email,
          role: e.role
        }
      }))
    } catch (e) {
      console.log(e);
      return this.sendError(res, 'error')
    }
  }

  /**
   * Edit a user
   *
   * @param req
   * @param res
   * @returns
   */
  static async editUserById(req: Request, res: Response) {
     const { email } = req.body;

    try {
      let user = await User.findOne({ _id: req.params.id });

      if (email && !user?.verifyEmail(email)) {
        return this.sendError(res, 'Email is not correct');
      }

      const userUpdated = await User.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true });

      user?.save((e) => console.log(e));
      return this.sendSuccessJSON(res, { userUpdated });
    } catch (e) {
      return this.sendError(res, "error");
    }
  }

  /**
   * Delete a user
   *
   * @param req
   * @param res
   * @returns
   */
  static async deleteUserById(req: Request, res: Response) {

    try {
      let user = await User.findOneAndDelete({ _id: req.params.id }).exec()

      if (!user) {
        return this.sendError(res, 'User not found')
      }

      this.sendSuccessJSON(res, { status: 'deleted' })

    } catch(e) {
      return this.sendError(res, "error")
    }
  }
}
