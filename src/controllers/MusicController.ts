import { Request, Response } from 'express'
import Music from '../models/Music'
import { UserRole } from '../types/enums'
import RequestController from './RequestController'
import dotenv from 'dotenv'
import { uploadFile, getFile } from '../services/AWS'
import { removeAllTmpUploads } from '../services/removeTmpUploads'
// import SpotifyWebApi from 'spotify-web-api-node'
dotenv.config()

// const spotifyApi = new SpotifyWebApi();
// spotifyApi.setAccessToken('BQAvr1tuFuJQMFamngGh_nsKFHl4lwCNtesLb06rd7Il5PA7mAlrzUwQxnjTX-i3VwIqKRbQ54GkCn3zIQU8BPYjRreAFJoDPqBkLXncxxRyesAqIhd15IpRZVziaq9Fi3k15CLRP8rSYUQ2F1MaPFg40BBq3QH8LtQp-dA8HN69eSbUrL1joOkgz0k6nOuV0rOfjiESLvuFdmnGMm7gwSxpumuXj17uWnZeA_B5n8OGR-fe_Fp6ujKWD-GZxEDxgUce36w3U3tw18_LAzP1DUT6BJjOx3QpRjOirIjDjzDiKj8A_vDO');

export default class MusicController extends RequestController {

    // static async profileID(req: Request, res: Response) {
    //     (async () => {
    //         const me = await spotifyApi.getMe();
    //         console.log(me.body);
    //         //console.log(me.body.id);
    //       })().catch(e => {
    //         console.error(e);
    //       });
    // }

  /**
   * Create & upload a music
   *
   * @param req
   * @param res
   * @returns
   */
    static async createMusic(req: Request, res: Response) {
      try {
        const musicData = JSON.parse(req.body.musicData);
        const music = await Music.create(musicData);
        const musicFile = req.file;

        const fileUploaded = await uploadFile(musicFile)!;

        music.path = fileUploaded.Location
        music.filename = fileUploaded.Key

        music.save()

        // to remove temporary files in uploads folder
        removeAllTmpUploads()

        return this.sendSuccessJSON(res, music);
      } catch (e) {
        console.log(e);
        return this.sendError(res, 'error');
      }
    }

    /**
     * Get all musics
     *
     * @param req
     * @param res
     * @returns
     */
    static async getMusics(req: Request, res: Response) {
      try {
        const { title, type, artists } = req.query;
        const filters = Object.entries({
          title: title && {$regex: title, $options: 'i'},
          type: type,
          ...(!!artists?.length) && {
            $and: (artists as string[]).map((artist: string) => ({artists: new RegExp((artist), 'i')}))
          },
        }).reduce((acc,[key, val]) => {
          const param = val && {[key]: val}
          return ({
            ...acc,
            ...param
          })
        },{});
        const musics =  await Music.find(filters);

        if (!musics) {
          return this.sendServerError(res);
        }

        this.sendSuccessJSON(
          res,
          musics
        );

      } catch (e) {
        console.log(e);
        return this.sendError(res, 'error');
      }
    }

    /**
     * Get a music
     *
     * @param req
     * @param res
     * @returns
    */
    static async getMusicById(req: Request, res: Response) {
      try {
        const music = await Music.findById(req.params.id);

        if (music) {
          return this.sendSuccessJSON(res, music);
        }

        return this.sendError(res, 'error');
      } catch (e) {
        console.log(e);
        return this.sendError(res, 'error');
      }
    }

    /**
     * Update a music
     *
     * @param req
     * @param res
     * @returns
     */
    static async updateMusicById(req: Request, res: Response) {
      try {
        const musicUpdated = await Music.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true });

        if (musicUpdated) {
          console.log(musicUpdated);
          return this.sendSuccessJSON(res, musicUpdated);
        }
        return this.sendError(res, 'error');
      } catch (e) {
        return this.sendError(res, 'error');
      }
    }

    /**
     * Delete a music
     *
     * @param req
     * @param res
     * @returns
     */
    static async deleteMusic(req: Request, res: Response) {
      try {
        const musicDeleted = await Music.deleteOne({ _id: req.params.id });

        // TODO : remove music in all playlist music field
        // TODO : remove music file

        if (musicDeleted) {
          return this.sendSuccessJSON(res, { status: 'deleted' });
        }
        return this.sendError(res, 'error');
      } catch (e) {
        console.log(e);
        return this.sendError(res, 'error');
      }
    }
}
