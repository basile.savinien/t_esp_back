import { Request, Response } from 'express';
import Playlist from '../models/Playlist';
import Room from '../models/Room';
import RequestController from './RequestController';
import User from "../models/User";

export default class RoomController extends RequestController {
  static async createRoom(req: Request, res: Response) {
    const {  isPrivate } = req.body;
    const id = res.locals.jwtPayload.id;

    try {
      let user = await User.findOne({ _id: id }).exec();
      if (!user) {
        return this.sendError(res, 'User not found');
      }
      const room = await Room.create({
        private: isPrivate,
        admin: user,
        config: {
          time: 20,
          started: false,
          playlist: null,
          finished: false
        },
        players: [{
          username: user.username,
          _id: user._id,
          avatar: user.avatar,
          score: 0
        }]
      });

      return this.sendSuccessJSON(res, room);
    } catch (e) {
      console.log(e);
      return this.sendError(res);
    }
  }

  static async addPlaylist(req: Request, res: Response) {
    const { room } = req.params;
    const { playlist } = req.body;

    try {
      const roomFound = await Room.findById(room);
      const playlistFound = await Playlist.findById(playlist);

      if (!roomFound || !playlistFound) {
        return this.sendError(res);
      }

      roomFound.playlist = playlistFound;
      await roomFound.save();

      return this.sendSuccessJSON(res, roomFound);
    } catch (e) {
      console.log(e);
      return this.sendError(res);
    }
  }

  static async getRooms(req: Request, res: Response) {
    try {
      const rooms = await Room.find();

      if (!rooms) {
        return this.sendServerError(res);
      }

      this.sendSuccessJSON(
        res,
        rooms.map((e) => {
          return {
            id: e.id,
            private: e.private,
          };
        })
      );
    } catch (e) {
      console.log(e);
      return this.sendError(res);
    }
  }

  static async getRoomById(req: Request, res: Response) {
    try {
      const room = await Room.findById(req.params.id);

      if (room) {
        return this.sendSuccessJSON(res, room);
      }

      return this.sendError(res);
    } catch (e) {
      console.log(e);
      return this.sendError(res);
    }
  }

  static async deleteRoom(req: Request, res: Response) {
    try {
      const roomDeleted = await Room.deleteOne({ _id: req.params.id });
      if (roomDeleted) {
        return this.sendSuccessJSON(res, { status: 'deleted' });
      }
      return this.sendError(res);
    } catch (e) {
      console.log(e);
      return this.sendError(res);
    }
  }

  static async enterInRoom(req: Request, res: Response) {
  }

  static async leaveRoom(req: Request, res: Response) {
  }
}
