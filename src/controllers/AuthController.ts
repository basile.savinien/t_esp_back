import { Request, Response } from 'express'
import User from '../models/User'
import { UserRole } from '../types/enums'
import RequestController from './RequestController'
import dotenv from 'dotenv'
import UserController from './UserController'
import {cleanUser} from "../helpers";

dotenv.config()

var SpotifyWebApi = require('spotify-web-api-node');

const scopes = [
  'ugc-image-upload',
  'user-read-playback-state',
  'user-modify-playback-state',
  'user-read-currently-playing',
  'streaming',
  'app-remote-control',
  'user-read-email',
  'user-read-private',
  'playlist-read-collaborative',
  'playlist-modify-public',
  'playlist-read-private',
  'playlist-modify-private',
  'user-library-modify',
  'user-library-read',
  'user-top-read',
  'user-read-playback-position',
  'user-read-recently-played',
  'user-follow-read',
  'user-follow-modify'
];

var spotifyApi = new SpotifyWebApi({
  clientId: process.env.CLIENT_ID,
  clientSecret: process.env.CLIENT_SECRET,
  redirectUri: 'http://localhost:8081/callback'
});

export default class AuthController extends RequestController {
  static async login(req: Request, res: Response) {
    const { email, password } = req.body

    if (!email || !password) {
      return this.sendError(res, 'Missing fields')
    }

    try {
      let user = await User.findOne({ email }).exec()

      if (!user) {
        return this.sendError(res, 'User not found')
      }

      const checkPassword = user.checkPassword(password)
      if (!checkPassword) {
        return this.sendError(res, 'Incorrect password!')
      }

      const accessToken = user.generateJWT()
      this.sendSuccessJSON(res, { accessToken, ...cleanUser(user) })
    } catch(e) {
      return this.sendError(res)
    }
  }

  static async register(req: Request, res: Response) {
    if (!req.body.email || !req.body.password || !req.body.username) {
      return res.json({ error: 'missing email, password or username' });
    }

    if (req.body.password.length < 8) {
      return res.json({ error: 'password need minimum 8 chars' });
    }

    const { username, email, password, role, avatar } = req.body

    try {
      let isUserExisting = await User.findOne({ email })
      isUserExisting = isUserExisting == null ? await User.findOne({ username }) : isUserExisting;

      if (isUserExisting) {
        return this.sendError(res, 'User already exists!')
      }

      const newUser = new User({
        username,
        email,
        password,
        role,
        avatar
      })

      await newUser.save()

      this.sendCreationSuccess(res, 'User created!')
    } catch(e) {
      console.log(e)
      this.sendServerError(res)
    }
  }

  static async spotify_login(req: Request, res: Response) {
    res.redirect(spotifyApi.createAuthorizeURL(scopes));
  }

  static async callback(req: Request, res: Response) {
    const error = req.query.error;
    const code = req.query.code;
    const state = req.query.state;

    if (error) {
      console.error('Callback Error:', error);
      res.send(`Callback Error: ${error}`);
      return;
    }

    spotifyApi
      .authorizationCodeGrant(code)
      .then((data: any) => {
        const access_token = data.body['access_token'];
        const refresh_token = data.body['refresh_token'];
        const expires_in = data.body['expires_in'];

        spotifyApi.setAccessToken(access_token);
        spotifyApi.setRefreshToken(refresh_token);

        //console.log('access_token:', access_token);
        //console.log('refresh_token:', refresh_token);

        // console.log(
        //   `Sucessfully retreived access token. Expires in ${expires_in} s.`
        // );
        res.redirect(`http://localhost:3000/spotifyCallback?auth=${access_token}`);

        setInterval(async () => {
          const data = await spotifyApi.refreshAccessToken();
          const access_token = data.body['access_token'];

          console.log('The access token has been refreshed!');
          console.log('access_token:', access_token);
          spotifyApi.setAccessToken(access_token);
        }, expires_in / 2 * 1000);
      })
      .catch((error: any) => {
        console.error('Error getting Tokens:', error);
        res.send(`Error getting Tokens: ${error}`);
      });
  }
}
