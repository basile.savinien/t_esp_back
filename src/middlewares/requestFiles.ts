import multer from 'multer';

const upload = multer({ dest: "uploads/tmp" });

export default upload
