import { Request, Response, NextFunction } from "express"
import { IUser } from "@/types/interfaces"

import User from '../models/User'
import { StatusCode, UserRole } from '../types/enums'

const checkRole = (roles: UserRole[]) => {
  return async (req: Request, res: Response, next: NextFunction) => {
    const id = res.locals.jwtPayload.id
    let user: Nullable<IUser> = null

    try {
      user = await User.findById(id)

      if (roles.includes(user!.role)) {
        next()
      } else {
        return res.status(StatusCode.UNAUTHORIZED).send('Unauthorized')
      }
    } catch(e) {
      return res.status(StatusCode.SERVER_ERROR).send('Server Error')
    }
  }
}

export default checkRole