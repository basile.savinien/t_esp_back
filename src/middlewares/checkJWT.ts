
import { Request, Response, NextFunction } from 'express'
import jwt from 'jsonwebtoken'
import config from '../config'
import { extractToken } from '../helpers'
import { StatusCode } from '../types/enums'

const checkJWT = (req: Request, res: Response, next: NextFunction) => {
  const token = extractToken(req)
  let jwtPayload: any

  try {
    jwtPayload = jwt.verify(token as string, config.jwt.secret)
    res.locals.jwtPayload = jwtPayload
  } catch (e) {
    return res.status(StatusCode.UNAUTHORIZED).send('Unauthorized')
  }

  next()
}

const checkAnonymousJWT = (req: Request, res: Response, next: NextFunction) => {
  const token = extractToken(req)

  if (token) {
    let jwtPayload = jwt.verify(token as string, config.jwt.secret)

    res.locals.jwtPayload = jwtPayload
  }
  next()
}

export {
  checkAnonymousJWT,
  checkJWT
}