FROM node:12.7-alpine

WORKDIR /app/server
COPY . /app/server

RUN npm install
RUN npm install pm2 -g

EXPOSE 8081

CMD npm run prod