# Launch Server (Dev)

```
Create a .env file and add these values: 
  - PORT: port the app will run on (optional, fallback to 8081)
  - DB_URI: MongoDB URI (mandatory)

$ npm run dev
```